package cache

import (
	"WB_level2/http/models"
	"sync"
)

type Cache struct {
	m    sync.Mutex
	pool map[string][]*models.Event
}

func NewCache() *Cache {
	return &Cache{
		m:    sync.Mutex{},
		pool: map[string][]*models.Event{},
	}
}
